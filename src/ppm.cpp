#include "ppm.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>

using namespace std;

Ppm::Ppm(){

}

Ppm::~Ppm(){

}

void Ppm::DecifraImagem(){
	char Comentarios[100];
	FILE *arquivo;
	FILE *arquivoTmp;
	int i, j, largura, altura;

	typedef struct {
			int r,g,b;
		}pixel;

		pixel imagem[largura][altura];

	arquivo = fopen(getEnderecoDoArquivo().c_str(), "r");

	while(arquivo==NULL){
		cout << "O endereço do arquivo não foi digitado corretamente. Tente novamente: " << endl;
		setEnderecoDoArquivo();
		arquivo = fopen(getEnderecoDoArquivo().c_str(), "r");
	}
	if(arquivo!=NULL){
		cout << "Arquivo Aberto com sucesso" << endl;
		fscanf(arquivo, "%s", NumeroMagico);
		fscanf(arquivo, "%s", Comentarios);
		fscanf(arquivo, "%d", &largura);
		fscanf(arquivo, "%d", &altura);
		fscanf(arquivo, "%d", &EscalaDeTons);

		for(i = 0; i < largura; i++){
			for(j = 0; j< altura; j++){
				fscanf(arquivo, "%d", &imagem[i][j].r);
				fscanf(arquivo, "%d", &imagem[i][j].g);
				fscanf(arquivo, "%d", &imagem[i][j].b);
			}
		}
		//arquivo >> Bytes;
		fclose(arquivo);

		arquivoTmp = fopen("arquivoTmp.ppm", "w");
		fprintf(arquivoTmp, "%s\n", NumeroMagico);
		fprintf(arquivoTmp, "%d %d\n", largura, altura);
		fprintf(arquivoTmp, "%d\n", EscalaDeTons);



		for(i = 0; i < largura; i++){
			for(j = 0; j < altura; j++){
				fprintf(arquivoTmp, "%d %d %d", EscalaDeTons - imagem[i][j].r, EscalaDeTons - imagem[i][j].g, EscalaDeTons - imagem[i][j].b);
			}
		}
		
		fclose(arquivoTmp);
		}
	}
	







































/*
int i, j;
		typedef struct 
		{
			int r;
			int g;
			int b;
		}pixel;

		pixel imagem[largura][altura], nova_imagem[largura][altura];

		cout << "Defina qual filtro será utilizado <r/g/b>: ";
		cin >> filtro;

		while(filtro != 'r' && filtro !='g' && filtro !='b'){
			cout << "O codigo do filtro (r/g/b) foi digitado de forma incorreta. Tente novamente digitando r, g ou b: ";
			cin >> filtro;
		}

		for(i = 0; i < largura; i++){
			for(j = 0; j < altura; j++){
				
					nova_imagem[i][j].r = EscalaDeTons - imagem[i][j].r;
					nova_imagem[i][j].g = EscalaDeTons - imagem[i][j].g;
					nova_imagem[i][j].b = EscalaDeTons - imagem[i][j].b;
					cout << nova_imagem[i][j];
				}
		}
		*/
