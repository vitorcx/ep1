#include "decifra.hpp"
#include <iostream>
#include <string.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

Decifra::Decifra(){
	EnderecoDoArquivo = "";
}

Decifra::~Decifra(){

}
void Decifra::ColheVariaveis(){
	string Hashtag, Comentarios;
	ifstream arquivo;

	arquivo.open(getEnderecoDoArquivo().c_str());

	while(arquivo.fail()){
		cout << "O endereço do arquivo não foi digitado corretamente. Tente novamente: " << endl;
		setEnderecoDoArquivo();
		arquivo.open(getEnderecoDoArquivo().c_str());
	}
	if(arquivo.is_open()){
		cout << "Arquivo Aberto com sucesso" << endl;
		arquivo >> NumeroMagico;
		arquivo >> Hashtag;
		arquivo >> EnderecoInicial;
		getline(arquivo, Comentarios);
		arquivo >> linhas;
		arquivo >> colunas;
		arquivo >> EscalaDeTons;
		arquivo >> Bytes;

		arquivo.close();
		
		//cout << "Numero Magico = " << NumeroMagico << endl << "Endereço inicial = " <<  EnderecoInicial << endl << "Numero de linhas = " << linhas << endl << "Numero de colunas = " << colunas << endl << "Escala de Tons = " << EscalaDeTons << endl;
	}		
}

string Decifra::getNumeroMagico(){
	return NumeroMagico;
}

int Decifra::getLinhas(){
	return linhas;
}

int Decifra::getColunas(){
	return colunas;
}

int Decifra::getEscalaDeTons(){
	return EscalaDeTons;
}

int Decifra::getEnderecoInicial(){
	return EnderecoInicial;
}

string Decifra::getBytes(){
	return Bytes;
}

void Decifra::setEnderecoDoArquivo(){
	cout << "Digite o Endereço do Arquivo: " << endl;
	cin >> EnderecoDoArquivo;
	this->EnderecoDoArquivo = EnderecoDoArquivo;
}
string Decifra::getEnderecoDoArquivo(){
	return EnderecoDoArquivo;
}

/*void Decifra::AbreArquivo(){
	ifstream arquivo;

	arquivo.open(getEnderecoDoArquivo().c_str());

	while(arquivo.fail()){
		cout << "O endereço do arquivo não foi digitado corretamente. Tente novamente: " << endl;
		setEnderecoDoArquivo();
		arquivo.open(getEnderecoDoArquivo().c_str());
	}
	if(arquivo.is_open()){
		cout << "Arquivo Aberto com sucesso" << endl;
	}

}*/