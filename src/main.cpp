#include <iostream>
#include "pgm.hpp"
#include "ppm.hpp"
#include <string>

using namespace std;

int main() {
	string formato;
	Ppm *Imagem1 = new Ppm();
	Pgm *Imagem2 = new Pgm();

	cout << "Especifique o formato da imagem <ppm/pgm> : ";
	cin >> formato;
	while(formato!="ppm" && formato!="pgm"){
		cout << "O formato não foi especificado corretamente. Digite ppm ou pgm: ";
		cin >> formato;
	}

	if(formato=="ppm"){
		Imagem1->setEnderecoDoArquivo();
		Imagem1->DecifraImagem();
	}
	else if(formato=="pgm"){
		Imagem2->setEnderecoDoArquivo();
		Imagem2->DecifraImagem();
	}

	delete(Imagem1);
	delete(Imagem2);

	return 0;
}