#ifndef PGM_HPP 
#define PGM_HPP
#include "decifra.hpp"

using namespace std;

class Pgm : public Decifra {
	public:
		Pgm();
		~Pgm();
		void DecifraImagem();
		void CriaArquivoTmp();

};

#endif