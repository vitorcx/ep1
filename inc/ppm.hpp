#ifndef PPM_HPP
#define PPM_HPP
#include "decifra.hpp"

using namespace std;

class Ppm : public Decifra {
	public:
		Ppm();
		~Ppm();
		void DecifraImagem();
};
#endif