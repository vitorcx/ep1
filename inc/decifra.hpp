#ifndef DECIFRA_HPP
#define DECIFRA_HPP
#include <string>

using namespace std;

class Decifra {
	protected:
		string EnderecoDoArquivo;
		char NumeroMagico[10];
		int EnderecoInicial;
		int linhas;
		int colunas;
		int EscalaDeTons;
		string Bytes;


	public:
		Decifra();
		~Decifra();
		string getNumeroMagico();
		string getBytes();
		int getEnderecoInicial();
		int getLinhas();
		int getColunas();
		int getEscalaDeTons();
		void setEnderecoDoArquivo();
		string getEnderecoDoArquivo();
		void ColheVariaveis();
};

#endif